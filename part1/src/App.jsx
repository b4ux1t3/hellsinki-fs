/* eslint-disable react/prop-types */
import { useState } from 'react';
import { Hello } from './Hello';

import './App.css'

export const Display = ({count}) => <div>{count}</div>

export const Button = ({onClick, text}) => <button onClick={onClick}>{text}</button>

export function App() {
  const [count, setCount] = useState(0);
  const [name, setName] = useState("Developer");

  const reset = () => setCount(0);
  const increment = () => setCount(count + 1);
  const decrement = () => setCount(count - 1);

  return (
    <>
      <Hello name={name} />
      <div className="card">
        <input
          type="text"
          name="name-input"
          id="name-input"
          onInput={(v) => setName(v.currentTarget.value)}
          placeholder='Developer' />
        <div className="flex-container row gap-2">
          <Button text="-" onClick={decrement} />
          <Button text="Reset" onClick={reset} />
          <Button text="+" onClick={increment} />

        </div>

        <Display count={count} />
      </div>


    </>
  );
}

export default App;
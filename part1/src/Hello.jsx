/* eslint-disable react/prop-types */

export const Hello = ({name}) =>
    <>
      <h1>Greetings, {name}</h1>
    </>

export default Hello;
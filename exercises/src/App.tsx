import { Content } from "./Content"
import { Header } from "./Header"
import { Total } from "./Total"

const App = () => {

  const course = 'Half Stack application development'
  const part1 = 'Fundamentals of React'
  const exercises1 = 10
  const part2 = 'Using props to pass data'
  const exercises2 = 7
  const part3 = 'State of a component'
  const exercises3 = 14


  const parts =[
    {title: part1, numExercises: exercises1},
    {title: part2, numExercises: exercises2},
    {title: part3, numExercises: exercises3},  
  ];

  return (
    <>
      <div>
        <Header course={course} />
        <Content parts={parts} />
        <hr />
        <Total numbers={parts.map(p => p.numExercises)} />
      </div>
    </>
  )
}

export default App

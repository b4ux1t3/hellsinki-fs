interface HeaderProp {
    course: string;
}

export const Header = (props: HeaderProp) => <h1>{props.course}</h1>;

type CoursePart = {
    title: string;
    numExercises: number;
};

interface ContentProp {
    parts: CoursePart[]
}

export const Content = (props: ContentProp) => 
    props.parts.map(e => <p> {e.title} {e.numExercises} </p>);

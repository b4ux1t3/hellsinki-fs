type TotalProp = {
    numbers: number[];
};

export const Total = (props: TotalProp) => 
    <p>Number of exercises {props.numbers.reduce((s, n) => s + n, 0)}</p>;

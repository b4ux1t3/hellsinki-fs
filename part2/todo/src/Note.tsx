export type CheckBoxChangedFunction = () => void
export type Guid = `${string}-${string}-${string}-${string}-${string}`;
export interface NoteProps {
    content: string;
    done: boolean;
    id: Guid;
    onChange: CheckBoxChangedFunction;
}
export const Note = (props: NoteProps) => {
    const className = props.done ? "done" : ""
    return <div className="row">
        <div className={className}>{props.content}</div>
        <input type="checkbox" name="isDone" id="isDone" checked={props.done} onChange={props.onChange}/>
    </div>
}
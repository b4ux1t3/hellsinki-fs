import { FormEvent, useState } from 'react'
import {Guid, Note} from './Note'
import './App.css'

interface Note {
  content: string;
  isDone: boolean;
  id: Guid;
}

const App = () => {
  
  const initialNotes: Note[] = [{content:"Woohoo", isDone: false, id:crypto.randomUUID()}];
  const [notes, setNotes] = useState(initialNotes);
  const [userInput, setUserInput] = useState(' a new note. . .');

  const addNote = (event: FormEvent<HTMLFormElement>) => {
    event?.preventDefault();
    setNotes([...notes, {content:userInput, isDone: false, id:crypto.randomUUID()}]);
    setUserInput('');
  }

  const markAsDone = (id: Guid) => {
    const newValues = notes.map(n => n.id === id ? {id: n.id, content: n.content, isDone: !n.isDone} : n);
    setNotes(newValues);
  }

  const handleNoteChange = (event: FormEvent<HTMLInputElement>) => setUserInput((event.target as HTMLInputElement).value);

  return (
    <>
      <h1>ToDo List</h1>
      <div className="card">
        {notes.map(n => <Note content={n.content} done={n.isDone} id={n.id} onChange={() => markAsDone(n.id)}></Note>)}
      </div>
      <form onSubmit={addNote}>
        <input 
          type="text" 
          onChange={handleNoteChange}
          value={userInput}/>
        <button type="submit">save</button>
      </form>
    </>
  )
}

export default App

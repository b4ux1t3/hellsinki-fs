import axios from "axios";
import { ContactInfo } from "../Contact";
import { Guid } from "../Guid";

const baseUrl = 'http://localhost:3000/persons';
export const getAll = () => 
    axios.get(baseUrl);

export const create = (newContact: ContactInfo) => 
    axios.post(baseUrl, newContact);

export const update = (id: Guid, updatedContact: ContactInfo) => 
    axios.put(`${baseUrl}/${id}`, updatedContact);

export const deleteContact = (id: Guid) =>
    axios.delete(`${baseUrl}/${id}`);
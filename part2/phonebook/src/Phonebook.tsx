import { FormEvent, useState } from "react";
import { Contact, ContactInfo } from "./Contact";

import "./Phonebook.css";
import { Guid } from "./Guid";

export type DeletePressedHandler = (id: Guid) => void

export interface PhonebookProps {
    contacts: ContactInfo[];
    deletePressed: DeletePressedHandler
}

export const Phonebook = (props: PhonebookProps) => {
    const [searchTerm, setSearchTerm] = useState('');
    const handleUpdateSearchTerm = (event: FormEvent<HTMLInputElement>) => 
        setSearchTerm((event.target as HTMLInputElement).value);

    let contacts = props.contacts;
    if (searchTerm.length > 0){
        contacts = contacts.filter(c => c.name.toLocaleLowerCase().includes(searchTerm!.toLocaleLowerCase()));
    }
    return (
        <>
            <div id="search">
                <input
                    type="text"
                    onChange={handleUpdateSearchTerm}
                    value={searchTerm}
                    id="search" 
                />
            </div>
            <hr/>
            <div id="list">
                {
                    contacts.map(p =>
                        <div className="contact-row">
                            <Contact 
                                contact={p}
                                deletePressed={props.deletePressed}
                            />
                        </div>
                    )
                }
            </div>
        </>
    )
}
import { FormEvent, useEffect, useState } from 'react'

import './App.css'
import { ContactInfo } from './Contact'
import { Phonebook } from './Phonebook';
import { create, deleteContact, getAll, update } from './services/server-update'
import { Guid } from './Guid';
const initialPersons: ContactInfo[] = [];

const phoneNumberRegex = /(\d{3}?-?\s*\d{3}-?\s*\d{4})|(\(\d{3}\)\s*\d{3}-?\s*\d{4})/;

const matchesPhoneNumber = (inp: string) => phoneNumberRegex.test(inp);

const App = () => {
  const [persons, setPersons] = useState(initialPersons)
  const [newContactName, setNewContactName] = useState("");
  const [newContactPhoneNumber, setNewContactPhoneNumber] = useState("");
  const [dataFetched, setDataFetched] = useState(false);
  useEffect(()  => {
      if (dataFetched) return;
      getAll()
        .then(resp => {
          setDataFetched(true);
          setPersons(resp.data);
        })
  }, [dataFetched]);

  const validateInput = (): boolean => {
    const matchesRegex = matchesPhoneNumber(newContactPhoneNumber);
    const hasName = newContactName.length > 0;
    const hasNumber = newContactPhoneNumber.length >  0;

    return matchesRegex && hasName && hasNumber;
  }

  const existingUser = (str: string) => 
    persons.some(p => p.name === str);

  const addContact = (event: FormEvent<HTMLFormElement>) => {
    event?.preventDefault();
    if (!validateInput()) return;
    if (existingUser(newContactName)) {
      const person = persons.find(p => p.name === newContactName);
      if (!person) {console.error(`This should be impossible. Name: ${newContactName}}`); return;}
      person!.phoneNumber = newContactPhoneNumber;
      update(person!.id, person!)
        .then(resp => {
          if (resp.status !== 200) return;
          setNewContactName('');
          setNewContactPhoneNumber('');
          setPersons(persons.map(p => p.id !== person!.id ? p : person!));
        });
      return;
    }

    const newPerson = { name: newContactName, phoneNumber: newContactPhoneNumber, id: crypto.randomUUID() };
    create(newPerson)
      .then(resp => {
        if (resp.status !== 204 && resp.status !== 201) return;
        setPersons([...persons, newPerson]);
        setNewContactName('');
        setNewContactPhoneNumber('');
      });
  };

  const deletePerson = (id: Guid) => {
    deleteContact(id)
      .then(resp => {
        if (resp.status !== 200) return;
        const contacts = persons.filter(c => c.id !== id);
        setPersons(contacts);
      })
  }

  const handleNewContactNameChanged = (event: FormEvent<HTMLInputElement>) => 
    setNewContactName((event.target as HTMLInputElement).value);
  
  const handleNewContactPhoneNumberChanged = (event: FormEvent<HTMLInputElement>) => {
    const val = (event.target as HTMLInputElement).value;
    setNewContactPhoneNumber(val);

    if (!matchesPhoneNumber(val)) {
      document.querySelector('#contact-number')?.classList.add('error')
    } else {
      document.querySelector('#contact-number')?.classList.remove('error')
    }
  }

  return (
    <>
      <div id="wrapper" className='border-1'>
        <div id="title">
          <h1>My Phonebook</h1>
        </div>
        <div id="input" className='card border-1'>
          <form onSubmit={addContact} className="flex col col-right gap-2">
            <div>
              <label htmlFor="contact-name"> Name: </label>
              <input
                type="text"
                onChange={handleNewContactNameChanged}
                value={newContactName}
                id="contact-name"/>
            </div>
            <div>
              <label htmlFor="contact-number"> Number: </label>
              <input
                type="text"
                onChange={handleNewContactPhoneNumberChanged}
                value={newContactPhoneNumber}
                id="contact-number" />
            </div>
            <button type="submit">save</button>
          </form>
        </div>
        <div id="phonebook" className="card border-1">
          <Phonebook
            contacts={persons}
            deletePressed={deletePerson}
          />
        </div>
      </div>
    </>
  );
}

export default App;

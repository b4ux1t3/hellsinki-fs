import './Contact.css'
import { Guid } from './Guid';

export interface ContactInfo {
    name: string;
    phoneNumber: string;
    id: Guid;
    
}

export interface ContactListProps {
    contact: ContactInfo;
    deletePressed: (id: Guid) => void;
}

export const Contact = (props: ContactListProps) => 
<div id="contact-wrapper">
    <div>{props.contact.name}</div>
    <div>{props.contact.phoneNumber}</div>
    <input type="button" value="❌" onClick={()=>props.deletePressed(props.contact.id)}/>
</div>
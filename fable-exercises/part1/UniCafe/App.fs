module UniCafe.App

open Fable.React
open Fable.React.Props
open Elmish
open Elmish.React

type Model = int * int * int
type Msg = 
    | Like
    | Dislike
    | Neutral
 
let model = Model (0, 0, 0)


let init () = model

let update msg model = 
    let (like, dislike, neutral) = match model with x, y, z -> x, y, z
    let newValues = 
        match msg with
        | Like -> (like + 1 , dislike, neutral)
        | Dislike -> (like, dislike + 1, neutral)
        | Neutral -> (like, dislike, neutral + 1)
    Model newValues

type ButtonDisplayProps = {
    amount: int
    buttonLabel: string
    onClick: Browser.Types.MouseEvent -> unit
}

let buttonDisplay props = 
    div [classList [("flex", true); ("column", true); ("centered", true)]] [
        button [OnClick props.onClick] [str props.buttonLabel]
        div [] [
            p [] [str $"%d{props.amount}"]
        ]
    ]

type ButtonsProps = {
    state: Model
    OnDislike: Browser.Types.MouseEvent -> unit
    OnNeutral: Browser.Types.MouseEvent -> unit
    OnLike: Browser.Types.MouseEvent -> unit
}

let Buttons props = 
    let (like, dislike, neutral) = props.state
    div [classList [("flex", true); ("row", true); ("gap-2", true)]] [
        buttonDisplay {
            amount = dislike
            onClick = props.OnDislike
            buttonLabel = "👎"
        }
        buttonDisplay {
            amount = neutral
            onClick = props.OnNeutral
            buttonLabel = "😐"
        }
        buttonDisplay {
            amount = like
            onClick = props.OnLike
            buttonLabel = "👍"
        }
    ]

type StatsProps = {
    like: int
    dislike: int
    neutral: int
}

let modelToStatsProp model =
    match model with
    | like, dislike, neutral -> {
        like = like 
        dislike = dislike 
        neutral = neutral
    }

let statistics props =
    let net = float <| (props.like - props.dislike)
    let all = props.like + props.dislike + props.neutral

    match all = 0 with
    | true -> p [] [str "No feedback given"]
    | false ->
        let average, positive =  
            match net > 0 with
            | true -> net / (float all), (float props.like) / (float all)
            | false -> 0.0, (float props.like) / (float all)
        div [] [
            p [] [str $"Average: %f{average}"]
            p [] [str $"Average positive: %f{positive}"]
        ]


let App model dispatch =
    div [classList [("flex", true); ("centered", true); ("column", true)]] [ 
        h1 [] [str "Leave us a rating!"]
        Buttons {
            state = model
            OnDislike = (fun _ -> dispatch Dislike)
            OnNeutral = (fun _ -> dispatch Neutral)
            OnLike = (fun _ -> dispatch Like)
            }
        model |> modelToStatsProp |> statistics 
    ] 

let view model dispatch = App model dispatch


Program.mkSimple init update view
|> Program.withReactBatched "root"
|> Program.run

module FableExercises.Total

type TotalProps = {
    numbers: int array
}


open Fable.React
let Total props = 
    let total = props.numbers |> Array.sum
    p [] [str $"Total exercises: %d{total }"]
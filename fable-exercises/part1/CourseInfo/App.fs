module FableExercises.App

open Fable.React
open Fable.Core
open Browser
open Header
open Content
open Total

type Model = { course: string; parts: ContentProps }

let model =
    { course = "Half-Stack Application Development"
      parts =
        { parts =
            [| { title = "Fundamentals of React"
                 numExercises = 10 }
               { title = "Using props to pass data"
                 numExercises = 7 }
               { title = "State of a component"
                 numExercises = 14 } |] } }


let init () = model

let update _ _ = init ()

let App model =
    div
        [ classList [ ("page-container", true) ] ]
        [

          model.course |> makeHeaderProps |> header


          model.parts |> Content

          hr []

          { numbers = model.parts.parts |> Array.map (fun m -> m.numExercises) } |> Total ]

let view model dispatch = App model

open Elmish
open Elmish.React

Program.mkSimple init update view
|> Program.withReactBatched "root"
|> Program.run

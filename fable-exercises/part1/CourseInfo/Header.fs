module FableExercises.Header

open Fable.React

type HeaderProps = {
    course: string
}

let makeHeaderProps s : HeaderProps = {course = s}
let header props = h1 [] [str props.course]
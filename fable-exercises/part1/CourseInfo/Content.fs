module FableExercises.Content

type PartProps = {
    title: string
    numExercises: int
}

type ContentProps = {
    parts: PartProps array
}

open Fable.React

let Part props = 
    div [classList [("border", true)]] [
        h2 [] [str $"%s{props.title}"] 
        p [] [str $"%d{props.numExercises} exercises"]
    ]

let Content props = 
    div [classList [("flex-container", true)]]
        [
            yield! (props.parts |> Array.map Part)
        ]
    
import { ReactElement, IHTMLProp } from "./fable_modules/Fable.React.Types.18.3.0/Fable.React.fs.js";
import * as react from "react";
import { keyValueList } from "./fable_modules/fable-library-ts.4.16.0/MapUtil.js";
import { ProgramModule_mkSimple, ProgramModule_run } from "./fable_modules/Fable.Elmish.4.0.0/program.fs.js";
import { Program_withReactBatched } from "./fable_modules/Fable.Elmish.React.4.0.0/react.fs.js";

export const model = "Sup.";

export function init(): string {
    return model;
}

export function update<$a, $b>(_arg: $a, _arg_1: $b): string {
    return init();
}

export function App(model_1: string): ReactElement {
    let props: Iterable<IHTMLProp>, children: Iterable<ReactElement>;
    const props_2: Iterable<IHTMLProp> = [];
    const children_2: Iterable<ReactElement> = [(props = [], (children = [model_1], react.createElement("p", keyValueList(props, 1), ...children)))];
    return react.createElement("div", keyValueList(props_2, 1), ...children_2);
}

export function view<$a>(model_1: string, dispatch: $a): ReactElement {
    return App(model_1);
}

ProgramModule_run<string, any, ReactElement>(Program_withReactBatched<void, string, any>("root", ProgramModule_mkSimple<void, string, any, ReactElement>(init, update, view)));


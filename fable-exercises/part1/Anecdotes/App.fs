module Anecdotes.App

open Fable.React

type Model = string

let model = "Sup."


let init () = model

let update _ _ = init ()


let App model =
    div [] [ p [] [str model] ] 

let view model dispatch = App model

open Elmish
open Elmish.React

Program.mkSimple init update view
|> Program.withReactBatched "root"
|> Program.run

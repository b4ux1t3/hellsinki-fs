#! /bin/sh

if [ -n $1 ] && [ -n $2 ]; then 
    mkdir -p "./fable-exercises/$1/$2"
    cd "./fable-exercises/$1/$2"
    dotnet new console --language F#
    dotnet new tool-manifest
    dotnet tool install fable
    dotnet add package Fable.Core
    dotnet add package Fable.Browser.Dom
    dotnet add package Fable.Elmish.React
    dotnet add package Fable.React


    npm init -y
    npm i -D vite
    npm i react-dom

    rm -rf Program*

    cd ../../..

    cp -r ./.vscode-template "./fable-exercises/$1/$2/.vscode"
    cp ./vite.config-template.ts "./fable-exercises/$1/$2/vite.config.ts"

    sed "s/REPLACEME/$2/" ./template.html > "./fable-exercises/$1/$2/index.html"
    sed "s/REPLACEME/$2/" ./template.fs > "./fable-exercises/$1/$2/App.fs"

    sed -i "s/Program/App/" ./fable-exercises/$1/$2/$2.fsproj
else
    echo 'Need a part and a directory name'
fi